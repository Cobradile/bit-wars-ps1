#include "constants.h"

void initialize();
void update();
void draw()

Color* color;

int main() {
	initialize();

	while(1) {
		update();
		clear_display();
		draw();
		display();
	}
}

void initialize() {
	initialize_heap();
	initialize_screen();
	// initializePad();
	color_create(0, 255, 0, &color);
	set_background_color(color);
	// audioInit();
}

void update() {
	padUpdate();
}

void draw() {
}
